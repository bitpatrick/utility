package it.prismaprogetti.academy.utility;

import java.util.regex.Pattern;

public class GestoreStringhe {

	/**
	 * rende maiuscola la prima lettera della stringa mentre le restanti le
	 * trasforma in minuscole
	 * 
	 * esempio: dE rOsSi -> De Rossi
	 * 
	 * @param stringaDaFormalizzare
	 * @return
	 */
	public static String correggiFormatoStringa(String stringa) {
		
		String stringaFormalizzata = "";

		String[] stringheDaFormalizzare = contaStringhe(stringa);

		for (int i = 0; i < stringheDaFormalizzare.length; i++) {

			Character primaLetteraStringa = Character.toUpperCase(stringheDaFormalizzare[i].trim().charAt(0));

			if (i == (stringheDaFormalizzare.length - 1)) {

				stringaFormalizzata += primaLetteraStringa
						+ stringheDaFormalizzare[i].trim().substring(1).toLowerCase();
				break;
			}

			stringaFormalizzata += primaLetteraStringa + stringheDaFormalizzare[i].trim().substring(1).toLowerCase()
					+ " ";

		}

		return stringaFormalizzata;

	}

	private static String[] contaStringhe(String stringa) {

		String stringaDaConteggiare = stringa.trim();

		String[] stringheConteggiate = stringaDaConteggiare.split("\\s+");

		return stringheConteggiate;

	}
	
	public static boolean soloCaratteri(String stringa) {
		
		if ( stringa == null ||Pattern.compile("\\d").matcher(stringa).find() )  {
			return false;
		}
		
		return true;
		
	}

}
