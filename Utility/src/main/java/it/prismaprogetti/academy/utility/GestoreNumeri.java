package it.prismaprogetti.academy.utility;

public class GestoreNumeri {

	public static boolean numeroInIntervallo(int min, int max, int numero) {

		if (min <= numero && numero <= max) {

			return true;
		}

		return false;
	}

	public static int generaNumeroCasuale(int min, int max) {

		return (int) Math.floor(Math.random() * (max - min + 1) + min);
	}

	public static int getNumberFromStringOrZero(String stringa) {
		
		int numero;
		try {
			numero = Integer.parseInt(stringa);

		} catch (NumberFormatException e) {
			numero = 0;
		}

		return numero;
	}

}
